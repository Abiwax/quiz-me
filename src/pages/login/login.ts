import {Component} from '@angular/core';
import {NavController, NavParams} from '@ionic/angular';
import {Router} from '@angular/router';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

import {Register} from '../register/register';
import {SignIn} from '../sign-in/sign-in';

import {AngularFireAuth} from '@angular/fire/auth';
import {Facebook} from '@ionic-native/facebook/ngx';
import * as firebase from 'firebase/app';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class Login {
    userProfile: any = null;

    constructor(private router: Router, public navCtrl: NavController, public navParams: NavParams, public angularFireAuth: AngularFireAuth, private facebook: Facebook) {

    }

    registerPage() {
        this.router.navigateByUrl('/register');
    }

    signinPage() {
        this.router.navigateByUrl('/sign-in');
    }

    googleSignIn() {
        this.angularFireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
        // this.af.auth.login({
        //   provider: AuthProviders.Facebook,
        //   method: AuthMethods.Popup,
        // }).then(
        //   (success) => {
        //     this.router.navigate(['/members']);
        //   }).catch(
        //   (err) => {
        //     this.error = err;
        //   })
    }

    facebookSignIn() {
        this.facebook.login(['email']).then((response) => {
            const facebookCredential = firebase.auth.FacebookAuthProvider
                .credential(response.authResponse.accessToken);

            firebase.auth().signInWithCredential(facebookCredential)
                .then((success) => {
                    console.log('Firebase success: ' + JSON.stringify(success));
                    this.userProfile = success;
                })
                .catch((error) => {
                    console.log('Firebase failure: ' + JSON.stringify(error));
                });

        }).catch((error) => {
            console.log(error);
        });

    }

    twitterSignIn() {
        this.angularFireAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider());
    }
}
