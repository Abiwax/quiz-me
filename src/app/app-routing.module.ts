import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('../pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'register', loadChildren: () => import('../pages/register/register.module').then( m => m.RegisterModule)},
  { path: 'sign-in', loadChildren: () => import('../pages/sign-in/sign-in.module').then( m => m.SignInModule)},
  { path: 'quiz', loadChildren: () => import('../pages/quiz/quiz.module').then( m => m.QuizModule)},
  { path: 'login', loadChildren: () => import('../pages/login/login.module').then( m => m.LoginModule)},
  { path: 'list', loadChildren: () => import('../pages/list/list.module').then( m => m.ListPageModule)},
  { path: 'category', loadChildren: () => import('../pages/category/category.module').then( m => m.CategoryModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
