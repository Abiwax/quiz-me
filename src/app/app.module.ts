import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';


import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {Register} from '../pages/register/register';
import {Login} from '../pages/login/login';
import {SignIn} from '../pages/sign-in/sign-in';
import {Quiz} from '../pages/quiz/quiz';
import {Category} from '../pages/category/category';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AngularFireAuthModule} from '@angular/fire/auth';

export const firebaseConfig = {
    apiKey: 'AIzaSyDYLOh9CgQNvH-n5-5-0mkg10OoEdVDAJ8',
    authDomain: 'externals-6b070.firebaseapp.com',
    databaseURL: 'https://externals-6b070.firebaseio.com',
    projectId: 'externals-6b070',
    storageBucket: 'externals-6b070.appspot.com',
    messagingSenderId: '902789597035'
};

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        ListPage,
        Quiz,
        Category,
        Register,
        SignIn,
        Login],
    entryComponents: [
        HomePage,
        ListPage,
        Quiz,
        Category,
        Register,
        SignIn,
        Login
    ],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        AngularFireAuthModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
